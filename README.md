# OAuth

Provisionamento de infraestrutura Keycloak com Docker-compose usando imagens Keycloak + MySQL (solução Keycloak) e Nginx + Certbot (Proxy reverso + SSL Let's Encrypt).

# Ingredientes

- Docker
- Docker-compose
- Domínio registrado no DNS para geração do certificado Let's Encrypt
- Scripts e arquivos desse projeto

# Modo de preparo

- Crie um arquivo em webserver/nginx/ com as configurações de proxy reverso do seu domínio
- Lembre-se que o domínio deve estar registrado no DNS
- Altere os arquivos init-letsencrypt.sh e respectivos server_name e diretórios do nginx
- Gere o certificado com o script init-letsencrypt.sh
- Atribua suas preferências nas variáveis dos serviços gerenciados pelo Docker Compose
- Suba a stack com: docker-compose up -d
- Seja feliz!
- Detalhes e alterações deixarei com você, fique a vontade para contribuir.